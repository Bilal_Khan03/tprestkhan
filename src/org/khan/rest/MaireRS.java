package org.khan.rest;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.khan.ejb.MaireEJB;
import org.khan.model.Maire;

@Path("maire")
public class MaireRS {

	
	
	@EJB
	private MaireEJB maireEJB;
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response findById(@PathParam("id") long id){				
		Maire maire = maireEJB.findById(id);
		if(maire==null)
			return Response.status(Status.NOT_FOUND).build();
		else 
			return Response.ok(maire).build();
	}	
		
	@POST @Path("create") @Produces(MediaType.APPLICATION_XML)
	public Response create(@FormParam("name") String name, 
			@FormParam("prenom") String prenom, 
			@FormParam("civilite") String civilite,
			@FormParam("date") String date) {
		Maire maire = maireEJB.create(name, prenom, civilite, date);
		return Response.ok(maire).build();
	}	
	
	@PUT
	@Path("update") 
	@Produces(MediaType.APPLICATION_XML)
	public Response update(@FormParam("name") String name, 
			@FormParam("prenom") String prenom, 
			@FormParam("civilite") String civilite,
			@FormParam("date") String date) {
		Maire maire = maireEJB.update(10, name, prenom, civilite, date);
		return Response.ok(maire).build();
	}		
	
	@DELETE
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response delete(@PathParam("id") long id) {

		Maire maire = maireEJB.delete(id);
		if (maire == null) {
			return Response.status(Status.NOT_FOUND).build();
		} else {
			return Response.ok(maire).build();
		}
	}
}