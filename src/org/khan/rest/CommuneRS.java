package org.khan.rest;

import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.khan.ejb.CommuneEJB;
import org.khan.model.Commune;

@Path("commune")
public class CommuneRS {

	@EJB
	private CommuneEJB communeEJB;
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response findById(@PathParam("id") long id){				
		Commune commune = communeEJB.findById(id);
		if(commune==null)
			return Response.status(Status.NOT_FOUND).build();
		else 
			return Response.ok(commune).build();
	}
	
	@POST
	@Path("create")
	@Produces(MediaType.TEXT_PLAIN)
	public Response create(
			@FormParam("code_INSEE") String code_INSEE, 
			@FormParam("code_postal") int code_postal, 
			@FormParam("coordonnees_gps") String coordonnees_gps,
			@FormParam("lgne_5") String lgne_5,
			@FormParam("libelle_acheminement") String libelle_acheminement,
			@FormParam("nom_commune")  String nom_commune) {
		
		Commune commune = communeEJB.create(code_INSEE, code_postal, coordonnees_gps, lgne_5, libelle_acheminement, nom_commune);
		return Response.ok(commune.getId()).build();	
	}
}
