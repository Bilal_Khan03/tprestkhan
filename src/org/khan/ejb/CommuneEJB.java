package org.khan.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.khan.model.Commune;

@Stateless
public class CommuneEJB {
	
	@PersistenceContext(unitName="tp-rest")
	private EntityManager em;
	
	public Commune findById(long id) {		
		Commune c = em.find(Commune.class,id);
		return c;
	}
	
	public  Commune create(String code_INSEE, int code_postal, String coordonnees_gps, String lgne_5,
			String libelle_acheminement, String nom_commune) {
		Commune commune = new Commune();
		commune.setNom_commune(nom_commune);
		commune.setCode_INSEE(code_INSEE);
		commune.setCode_postal(code_postal);
		commune.setCoordonnees_gps(coordonnees_gps);
		commune.setLgne_5(lgne_5);
		commune.setLibelle_acheminement(libelle_acheminement);
		em.persist(commune);
		return commune;
	}

}
