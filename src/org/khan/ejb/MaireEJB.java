package org.khan.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.khan.model.Maire;

@Stateless
public class MaireEJB {
	
	@PersistenceContext(unitName="tp-rest")
	private EntityManager em;
	
	public Maire findById(long id) {
		
		Maire m = em.find(Maire.class,id);
		return m;
	}

	public  Maire create(String name, String prenom,String civilite, String date) {
		Maire maire = new Maire();
		maire.setNom(name);
		maire.setPrenom(prenom);
		maire.setCivilite(civilite);
		maire.setDate_naissance(date);
		em.persist(maire);
		return maire;
	}
	
	public Maire update(long id, String nom, String prenom, String civilite, String date){
		Maire maire = em.find(Maire.class,id);		
		if(nom != null)
			maire.setNom(nom);
		if(prenom!= null)
			maire.setPrenom(prenom);
		if(civilite!= null)
			maire.setCivilite(civilite);
		if(date!= null)
			maire.setDate_naissance(date);	
		return maire;
	}
		
	public Maire delete(long id){
		Maire maire = findById(id);
		if(maire != null)
			em.remove(maire);
		return maire;
	}
}
