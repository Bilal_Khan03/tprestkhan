package org.khan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="commune")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name="communes")
public class Commune {	
	public Commune() {
	}

	@XmlAttribute
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "Code_commune_INSEE")
	private String code_INSEE;
	
	@Column(name = "Nom_commune" , length = 40)
	private String nom_commune;
	
	@Column(name = "Code_postal")
	private int code_postal;
	
	@Column(name = "Libelle_acheminement")
	private String libelle_acheminement;

	@Column(name = "Ligne_5")
	private String lgne_5;
	
	@Column(name = "coordonnees_gps")
	private String coordonnees_gps;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode_INSEE() {
		return code_INSEE;
	}

	public void setCode_INSEE(String code_INSEE) {
		this.code_INSEE = code_INSEE;
	}

	public String getNom_commune() {
		return nom_commune;
	}

	public void setNom_commune(String nom_commune) {
		this.nom_commune = nom_commune;
	}

	public int getCode_postal() {
		return code_postal;
	}

	public void setCode_postal(int code_postal) {
		this.code_postal = code_postal;
	}

	public String getLibelle_acheminement() {
		return libelle_acheminement;
	}

	public void setLibelle_acheminement(String libelle_acheminement) {
		this.libelle_acheminement = libelle_acheminement;
	}

	public String getLgne_5() {
		return lgne_5;
	}

	public void setLgne_5(String lgne_5) {
		this.lgne_5 = lgne_5;
	}

	public String getCoordonnees_gps() {
		return coordonnees_gps;
	}

	public void setCoordonnees_gps(String coordonnees_gps) {
		this.coordonnees_gps = coordonnees_gps;
	}

	@Override
	public String toString() {
		return "Commune [id=" + id + ", code_INSEE=" + code_INSEE + ", nom_commune=" + nom_commune + ", code_postal="
				+ code_postal + ", libelle_acheminement=" + libelle_acheminement + ", lgne_5=" + lgne_5
				+ ", coordonnees_gps=" + coordonnees_gps + "]";
	}	
	
	
	
}