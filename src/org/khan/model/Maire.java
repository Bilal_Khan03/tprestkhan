package org.khan.model;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name="maire")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name="Maires")
public class Maire {

	
	public Maire() {
	}

	@XmlAttribute
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	
	@Column(name="Nom_elu", length=40)
	private String nom;

	@Column(name="Prenom_elu", length=40)
	private String prenom;
	
	@Column(name="Civilite", length=40)
	private String civilite;
	
	@Column(name="Date_naissance", length=40)
	private String date_naissance;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}

	@Override
	public String toString() {
		return "Maire [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", civilite=" + civilite
				+ ", date_naissance=" + date_naissance + "]";
	}

	
	
}
