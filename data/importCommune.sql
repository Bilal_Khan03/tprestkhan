use `base_rest`;
DROP TABLE IF EXISTS Communes;
CREATE TABLE Communes
  (
  id BIGINT(20) NOT NULL auto_increment KEY,
  Code_commune_INSEE TEXT NOT NULL,
  Nom_commune TEXT,
  Code_postal INT NOT NULL ,
  Libelle_acheminement TEXT,
	Ligne_5 TEXT,
    coordonnees_gps TEXT
);

LOAD DATA LOCAL INFILE 'C:/Users/Telecom/Desktop/RestWorkspace/TpRest/data/laposte_hexasmal.csv'
 INTO TABLE Communes FIELDS TERMINATED BY ';' ENCLOSED BY '"' 
 ESCAPED BY '\\'
 LINES
	STARTING BY ''
	TERMINATED BY '\n'
 IGNORE 1 LINES
 (Code_commune_INSEE, Nom_commune, Code_postal, Libelle_acheminement, Ligne_5, coordonnees_gps);
 
 