use `base_rest`;
DROP TABLE IF EXISTS Maires;
CREATE TABLE Maires
  (
  id BIGINT(20) NOT NULL auto_increment Primary KEY,
  `Nom_elu` TEXT NOT NULL,
  `Prenom_elu` TEXT NOT NULL,
  `Civilite` TEXT NOT NULL ,
  `Date_naissance` TEXT NOT NULL
)ENGINE=InnoDB DEFAULT CHARACTER SET=latin1;

LOAD DATA LOCAL INFILE 'C:/Users/Telecom/Desktop/RestWorkspace/TpRest/data/maires-25-04-2014.csv'
 INTO TABLE Maires CHARACTER SET latin1 FIELDS TERMINATED BY ';' ENCLOSED BY '"' 
 ESCAPED BY '\\'
 LINES
	STARTING BY ''
	TERMINATED BY '\n'
 IGNORE 4 LINES
 ( @dummy,  @dummy,  @dummy,  @dummy,  @dummy, `Nom_elu`, `Prenom_elu`, `Civilite`, `Date_naissance`,  @dummy,  @dummy);

 
SET SQL_SAFE_UPDATES=0;
UPDATE maires SET Date_naissance = STR_TO_DATE(Date_naissance, '%d/%m/%Y %H:%i');
SET SQL_SAFE_UPDATES=1;